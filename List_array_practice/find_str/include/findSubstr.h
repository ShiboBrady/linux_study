/*************************************************************************
	> File Name: findSubstr.h
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 07:47:27 AM PDT
 ************************************************************************/

#ifndef FINDSUBSTR_H
#define FINDSUBSTR_H

#include <stdio.h>
#include <string.h>

int findSubstr(char *str, char *sub);

#endif
