/*************************************************************************
	> File Name: findSubstr.c
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 07:33:08 AM PDT
 ************************************************************************/

#include <stdio.h>
#include <string.h>

int findSubstr(char *str, char *sub)
{
	int i, j, strLength, subLength;
	strLength=strlen(str);
	subLength=strlen(sub);
	if(subLength > strLength){
		return -1;
	}
	for(i = 0; i < strLength - subLength; ++i){
		for(j = 0; j < subLength; ++j){
			if(str[i+j] != sub[j]){
				break;
			}
		}
		if(j == subLength){
			return i;
		}
	}
}
