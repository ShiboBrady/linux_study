/*************************************************************************
	> File Name: main.c
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 07:49:18 AM PDT
 ************************************************************************/

#include "findSubstr.h"

int main()
{
	char str[100], sub[100];
	printf("Please input a string:\n");
	gets(str);
	printf("Please input a substring:\n");
	gets(sub);
	int position = findSubstr(str,sub);
	if(position == -1){
		printf("Isn't substring.\n'");
	}
	else{
		printf("The position is %d.\n",position);
	}
	return 0;
}
