/*************************************************************************
	> File Name: intToStr.h
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 08:15:17 AM PDT
 ************************************************************************/

#ifndef INTTOSTR_H
#define INTTOSTR_H

#include <stdio.h>
#include <string.h>

void intToStr(int a, char *str);

#endif
