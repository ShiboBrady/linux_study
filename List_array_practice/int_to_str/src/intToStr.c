/*************************************************************************
	> File Name: intToStr.c
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 08:01:07 AM PDT
 ************************************************************************/

#include<stdio.h>

void intToStr(int a, char *ch)
{
	int abs, val = a;
	char *str = ch, temp;
	abs = a > 0 ? a : -a;
	do
		*(str++) = abs % 10 + 48;
	while(abs /= 10);
	if(val < 0){
		*(str++) = '-';
	}
	*str = 0;
	--str;
	while(ch < str){
		temp = *ch;
		*ch = *str;
		*str = temp;
		++ch;
		--str;
	}
}
