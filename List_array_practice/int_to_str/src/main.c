/*************************************************************************
	> File Name: main.c
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 08:17:02 AM PDT
 ************************************************************************/

#include "intToStr.h"

int main()
{
	int a;
	char str[20];
	printf("Please input a number:\n");
	scanf("%d",&a);
	intToStr(a,str);
	puts(str);
	return 0;
}

