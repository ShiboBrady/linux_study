/*************************************************************************
	> File Name: list_struct.h
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 06:21:31 AM PDT
 ************************************************************************/

#ifndef LIST_STRUCT_H
#define LIST_STRUCT_H

#include <stdio.h>
#include <stdlib.h>

typedef struct listNode
{
	int data;
	struct listNode *next;
}Node ,*pNode;

void initList(pNode *h);
void insertNode(pNode *h,int n);
void showList(char *str,pNode h);
void mergeList(pNode *h1,pNode *h2);

#endif
