/*************************************************************************
	> File Name: list_operation.c
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 06:23:44 AM PDT
 ************************************************************************/
#include "list_struct.h"

void initList(pNode *h)
{
	*h=NULL;
}

void insertNode(pNode *h,int n)
{
	pNode p,q,r;
	r=(pNode)calloc(1,sizeof(Node));
	r->data=n;
	r->next=NULL;
	p=q=*h;

	if(*h==NULL){
		*h=r;
	}
	else{
		while(p->next && p->data < n){
			q = p;
			p = p->next;
		}
		if(p->data >= n){
			if(p == q){
				r->next = *h;
				*h = r;
			}
			else{
				q->next = r;
				r->next = p;
			}
		}
		else{
			p->next=r;
		}
	}
}

void showList(char *str,pNode h)
{
	puts(str);
	while(h){
		printf("%d ",h->data);
		h = h->next;
	}
	printf("\n");
}


