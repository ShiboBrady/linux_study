/*************************************************************************
	> File Name: main.c
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 06:50:22 AM PDT
 ************************************************************************/

#include "list_struct.h"
#define N 10
int main()
{
	pNode list1,list2;
	initList(&list1);
	initList(&list2);
	int i, elem;
	printf("Please input %d numbers for list1:\n");
	for(i = 0; i < N; ++i){
		scanf("%d", &elem);
		insertNode(&list1, elem);
	}
	printf("Please input %d numbers fot list2:\n");
	for(i = 0; i < N; ++i){
		scanf("%d", &elem);
		insertNode(&list2, elem);
	}
	showList("List1",list1);
	showList("List2",list2);
	mergeList(&list1,&list2);
	showList("After mergeing",list1);
	return 0;
}
