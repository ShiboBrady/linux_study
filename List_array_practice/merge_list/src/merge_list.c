/*************************************************************************
	> File Name: merge_list.c
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 06:33:56 AM PDT
 ************************************************************************/

#include "list_struct.h"

void mergeList(pNode *list1,pNode *list2)
{
	pNode pa1, pa2, pb1, pb2;
	pa1 = pa2 = *list1;
	pb1 = pb2 = *list2;
	while(pa1 && pb1){
		while(pa1->next && pa1->data < pb1->data){
			pa2 = pa1;
			pa1 = pa1->next;
		}
		if(pa1->data >= pb1->data){
			if(pa1 == pa2){
				*list1 = pb1;
			}
			else{
				pa2->next = pb1;
			}
			pb1 = pb1->next;
			pb2->next = pa1;
			pa2 = pb2;
			pb2 = pb1;
		}
		else{
			pa2 = pa1;
			pa1 = pa1->next;
		}
	}
	if(pa1 == NULL && pb1){
		pa2->next = pb1;
	}
}
