/*************************************************************************
	> File Name: float_to_str.h
	> Author: ZhangShibo
	> Mail:453430198@qq.com 
	> Created Time: Thu 19 Jun 2014 01:46:35 AM PDT
 ************************************************************************/

#ifndef FLOAT_TO_STR_H
#define FLOAT_TO_STR_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void float_to_str(char *str, float fl);

#endif
